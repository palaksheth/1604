#!/usr/bin/perl

## Usage: perl generate_RAWCOUNTS_SGE.pl --indir /path/to/run [--outdir /path/to/outfolder] [--cpus cpus/job] [--Qmin N] [--MAPQmin N] [--mismatch 0.N] [--percentBases N] [--bwaMaxGaps N] [--bwaMaxGapExts N] [--bwaGapOpenPen N] [--bwaGapExtPen N] [--bwaMismatchPen N]

use strict;
use warnings;
use File::Basename;
use Cwd qw(abs_path cwd);
my $scriptDir = "";
BEGIN {
	$scriptDir = abs_path(dirname($0));
	unshift @INC, "$scriptDir";
	#$scriptDir = "$scriptDir/scripts";
	unshift @INC, "$scriptDir/scripts";
}
use Schedule::SGELK;
use File::Tee qw(tee);
use File::Path 'make_path';
use Parallel::Loops;
use List::MoreUtils qw(uniq);
use String::Approx 'aindex';
use Getopt::Long; 
use DateTime;
use DateTime::Format::Human::Duration;

print "\n";
print qx/ps -o args $$/;
print "\n\n";

my $dtStart = DateTime->now;

## load commandline input variables
my %inputs = (); 
GetOptions( \%inputs, 'help|h', 'indir:s', 'outdir:s', 'cpus:i', 'Qmin:i', 'MAPQmin:i', 'mismatch:f', 'percentBases:i', 'bwaMaxGaps:i', 'bwaMaxGapExts:i', 'bwaGapOpenPen:i', 'bwaGapExtPen:i', 'bwaMismatchPen:i', 'mtName', 'wtName', 'allName'); 

# if help is requested
if ($inputs{help}) {
	Usage();
	exit 0;
}

## predefined paths for programs and files
my %paths;
# programs
$paths{CUTADAPT} = "$scriptDir/bin/cutadapt";
#my $CUTADAPT = "$scriptDir/bin/cutadapt";
$paths{FASTQ_QUAL_FILTER} = "$scriptDir/bin/fastq_quality_filter";
#my $FASTQ_QUAL_FILTER = "$scriptDir/bin/fastq_quality_filter";
$paths{BWA} = "$scriptDir/bin/bwa-0.7.12/bwa";
#my $BWA = "$scriptDir/bin/bwa-0.7.12/bwa";
$paths{SAMTOOLS} = "$scriptDir/bin/samtools-1.3.1/samtools";
#my $SAMTOOLS = "$scriptDir/bin/samtools-1.3.1/samtools";
$paths{REF} = "$scriptDir/ref/hg19/hg19_primary_chromosomes.fa";
#my $REF = "$scriptDir/ref/hg19/hg19_primary_chromosomes.fa";
$paths{BAMREADCOUNT} = "$scriptDir/bin/bam-readcount";
#my $BAMREADCOUNT = "$scriptDir/bin/bam-readcount";
$paths{FASTQC} = "$scriptDir/bin/FastQC/fastqc";
#my $FASTQC = "$scriptDir/bin/FastQC/fastqc";
# files
$paths{adaptersFile} = "$scriptDir/data/trovSeqsToTrim.txt";
#my $adapters = "$scriptDir/data/trovSeqsToTrim.txt";
$paths{primersFile} = "$scriptDir/data/trovPrimersToFlag.txt";
#my $primerFile = "$scriptDir/data/trovPrimersToFlag.txt";
$paths{targetsFile} = "$scriptDir/data/trovTargetedRegions.bed";
#my $targetsFile = "$scriptDir/data/trovTargetedRegions.bed";
# scripts
$paths{PLOT} = "Rscript $scriptDir/scripts/generateSTDplots_fromRAWCOUNTS.R";
#my $PLOT = "Rscript $scriptDir/scripts/generateSTDplots_fromRAWCOUNTS.R";


## configure command line options
# input dir
if (!defined $inputs{indir} || !-d $inputs{indir}) {
	Usage();
	die "ERROR: --indir must be defined! IE: path to Illumina Run Folder: /mnt/clia/runs/160517_M02001_0491_000000000-AP8E6\n";
}
my $inDir = "$inputs{indir}/";
my $sampleSheetFile = "$inputs{indir}/SampleSheet.csv";
if ($inDir !~ m/basecalls/i) {
	$inDir = $inDir . "/Data/Intensities/BaseCalls/";
}
else {
	$sampleSheetFile =~ s/\/Data\/Intensities\/BaseCalls\///;
}
$inDir = abs_path(glob($inDir));
$sampleSheetFile = abs_path(glob($sampleSheetFile));
my @s = split("/",$inDir);
my $runId = $s[-4];

# output dir
my $outDir="";
my $tempDir="";
if (defined $inputs{outdir}) {
	print "Using provided output folder location: $inputs{outdir}\n";
	$outDir = "$inputs{outdir}/$runId";
}
else {
	print "Output folder undefined! Outputting to folder \"count_results\" in current working directory...\n";
	$outDir = cwd();
	$outDir = "$outDir/count_results/$runId";
}
if (-d $outDir) {
	die "ERROR: Output directory $outDir already exists! Delete directory or choose new output path!\n";
}
else {
	make_path $outDir or die "ERROR: Failed to create output path $outDir\n";
	$tempDir = "$outDir/temp";
	make_path $tempDir or die "ERROR: Failed to create temp folder path $tempDir\n";
}
$outDir = abs_path(glob($outDir));
$tempDir = abs_path(glob($tempDir));
print "Output directory: $outDir\n";
print "\tTemp directory: $tempDir\n";

# other command line options
my $Qmin = 20;
if (defined $inputs{Qmin}) {
	$Qmin = int($inputs{Qmin});
}
my $cpuCount = 32;
$cpuCount = int($cpuCount);
if (defined $inputs{cpus}) {
	$cpuCount = int($inputs{cpus});
}
my $MAPQmin = 25;
if (defined $inputs{MAPQmin}) {
	$MAPQmin = int($inputs{MAPQmin});
}
my $mismatch = 0.15;
if (defined $inputs{mismatch}) {
	if ($inputs{mismatch}>=0 && $inputs{mismatch}<1) {
		$mismatch = $inputs{mismatch};
	}
	else {
		die "ERROR: --mismatch value must be >=0 and <1!\n";
	}
}
my $percentBases = 100;
if (defined $inputs{percentBases}) {
	if ($inputs{percentBases}>0 && $inputs{percentBases}<=100) {
		$percentBases = $inputs{percentBases};
	}
	else {
		die "ERROR: --percentBases value must be >0 and <=100!\n";
	}
}
#'bwaMaxGaps:i', 'bwaMaxGapExts:i', 'bwaGapOpenPen:i', 'bwaGapExtPen:i', 'bwaMismatchPen:i'
my $bwaMaxGaps = 25;
if (defined $inputs{bwaMaxGaps}) {
	$bwaMaxGaps = $inputs{bwaMaxGaps};
}
my $bwaMaxGapExts = 25;
if (defined $inputs{bwaMaxGapExts}) {
	$bwaMaxGapExts = $inputs{bwaMaxGapExts};
}
my $bwaGapOpenPen = 0;
if (defined $inputs{bwaGapOpenPen}) {
	$bwaGapOpenPen = $inputs{bwaGapOpenPen};
}
my $bwaGapExtPen = 0;
if (defined $inputs{bwaGapExtPen}) {
	$bwaGapExtPen = $inputs{bwaGapExtPen};
}
my $bwaMismatchPen = 3;
if (defined $inputs{bwaMismatchPen}) {
	$bwaMismatchPen = $inputs{bwaMismatchPen};
}


### DO STUFF ###
print "\n";
print "Processing BaseCalls directory: $inDir\n";
if (-e $sampleSheetFile) { print "SampleSheet: $sampleSheetFile\n"; system("cp $sampleSheetFile $outDir/");} else { die "ERROR: Could not locate samplesheet at $sampleSheetFile: $!\n"; }

# load assay targets file
$targetsFile = abs_path(glob($targetsFile));
open (TARGETS, '<', $targetsFile);
chomp( my @targets = <TARGETS> );
close TARGETS;
# get assay type from sampleSheet
my $sampleAssayHashRef = getAssayFromSampleSheetFile($sampleSheetFile);
my %sampleAssayHash = %$sampleAssayHashRef;
# initialize cacheOutHash
my %cacheOutHash;
my @assays;
foreach my $key (keys %sampleAssayHash) {
	push @assays, $sampleAssayHash{$key}{'assay'};
}
@assays = uniq(@assays);
my @inputs;
foreach my $key (keys %sampleAssayHash) {
	push @inputs, $sampleAssayHash{$key}{'input'};
}
@inputs = uniq(@inputs);




#foreach my $assay (@assays) {
	#my @array = ();
	#$cacheOutHash{$assay} = \@array;
#}


# find and load fastq files
my @fastqFileNames;
my @fastqFilePaths;
# find fastq.gz files in BaseCalls dir
opendir(DIR, $inDir) or die "ERROR: $!\n";
my @files = sort {(stat "$inDir/$b")[7] <=> (stat "$inDir/$a")[7]} readdir(DIR); # try to process large files first
foreach my $file (@files) {
	# We only want files
	next unless (-f "$inDir/$file");
	# Use a regular expression to find files ending in .fastq.gz
    next unless ($file =~ m/\.fastq.gz$/);
    # Use a regex to find files that do not have index read or undetermined or previously processed
    next if ($file =~ m/I1|Undetermined|_trimmed/i);
	# push filenames and paths
	push (@fastqFileNames, $file);
	$file = abs_path("$inDir/$file");
	push (@fastqFilePaths, $file);	
}
closedir(DIR);


# set up variables for parallel processing
my $pl = Parallel::Loops->new(4);
#my @cacheOut;
my %mutHeaderHash;
$pl->share(\@fastqFileNames);
$pl->share(\@fastqFilePaths);
#$pl->share(\@cacheOut);
$pl->share(\%cacheOutHash);
$pl->share(\%mutHeaderHash);
$pl->share(\%sampleAssayHash);
$pl->share(\@targets);

# process fastq files in parallel fashion
$pl->foreach (\@fastqFilePaths, sub {
	sleep(int(rand(30)));
	
	my $fastqOrig = $_;
	my $name = basename($fastqOrig,".fastq.gz");
	# set up log file for sample
	my $log_file = "$outDir/$name"."_log.txt";
	tee(STDOUT, '>>', "$log_file");
	tee(STDERR, '>>', "$log_file");
	
	my ($sampleName, $sampleNumber) = getFastqInfo($name);
	
	## get target info based on sampleNumber
	my $assay = $sampleAssayHash{$sampleNumber}{'assay'};
	my $input = $sampleAssayHash{$sampleNumber}{'input'};
	
	# get target based on assay
	my ($target, $wtSeq) = getTargetFromAssay($assay);
	@s = split(/:/,$target);
	my @t = split(/-/,$s[1]);
	my $targetStart = $t[0];
	my $targetEnd = $t[1];
	
	# load mutants based on assay and target
	my ($mtCoordRef, $mtCoordsFile) = getMutantsCoords($assay);
	my @mutantsCoordsArr = @$mtCoordRef;
	my %mutCoordsCount;
		
	print "Processing sample: $name\n";	
	my $fastq = $fastqOrig;
	
	if ($assay ne "NA" && $target ne "NA") {
		print "\tDetected Assay: $assay Target: $target wtSeq: $wtSeq Input: $input\n";
	}
	else {
		die "ERROR: Could not determine assay from $sampleSheetFile\n";
	}
	
	# find expected CN from name if possible
	my $expectedCN = "";
	if ($name =~ m/(^STD|^AV)/i) {
		$expectedCN = $sampleName;
		my @s = split("-",$expectedCN);
		$expectedCN = $s[0];
		$expectedCN =~ s/\D+//g;
	}
	elsif ($name =~ m/(^CTL)/i) {
		if ($name =~ m/(CTLn)/i) {
			$expectedCN = 0;
		}
		else {
			$expectedCN = $sampleName;
			my @s = split("-",$expectedCN);
			$expectedCN = $s[0];
			$expectedCN =~ s/\D+//g;
		}
	}
	elsif ($name =~ m/(^NTC)/i) {
		$expectedCN = 0;
	}
	elsif ($name =~ m/^(\d+)MT/i) {
		$expectedCN = $sampleName;
		my @s = split("-",$expectedCN);
		$expectedCN = $s[0];
		$expectedCN =~ s/\D+//g;
	}
	
	
	
	# count raw reads in fastq
	my $rawReads = 0;
	my $CMD = "zcat $fastq | wc -l";
	$rawReads = qx($CMD);
	#my @s = split(/\s+/, $rawReads);
	$rawReads = int($rawReads/4); # each fastq record is 4 lines
	print "\tRaw Reads: $rawReads\n";
	
	$fastq =~ s/.fastq.gz/_trimmed.fastq.gz/;
	my $fastqTrim = $fastq;
	$fastqTrim =~ s/_trimmed.fastq.gz/_trimmed_minQ$Qmin.fastq.gz/;
	
	# trim adapters	
	# cutadapt -a ADAPTER [options] [-o output.fastq] input.fastq
	$CMD = "$CUTADAPT -m 10 -e $mismatch -n 2 -b file:$adapters -o $fastq $fastqOrig";
	print "\tRunning command: $CMD\n";
	print "\n";
	system($CMD);
	
	# count reads in trimmed-only fastq
	my $trimmedReads = 0;
	$CMD = "zcat $fastq | wc -l";
	$trimmedReads = qx($CMD);
	#@s = split(/\s+/, $totalReads);
	#$totalReads = int($s[0]/4); # each fastq record is 4 lines
	$trimmedReads = int($trimmedReads/4); # each fastq record is 4 lines
	print "\tTotal Reads in trimmed FASTQ: $trimmedReads\n";

	#filter out reads with ANY bases qscore less than Qmin
	#usage: fastq_quality_filter [-h] [-v] [-q N] [-p N] [-z] [-i INFILE] [-o OUTFILE]
	$CMD = "zcat $fastq | $FASTQ_QUAL_FILTER -Q33 -v -q $Qmin -p $percentBases -z -o $fastqTrim";
	print "\tRunning command: $CMD\n";
	print "\n";
	system($CMD);
	#sleep(60);
	$fastq = $fastqTrim;
	
	# count reads in filtered trimmed fastq
	my $totalReads = 0;
	$CMD = "zcat $fastq | wc -l";
	$totalReads = qx($CMD);
	#@s = split(/\s+/, $totalReads);
	#$totalReads = int($s[0]/4); # each fastq record is 4 lines
	$totalReads = int($totalReads/4); # each fastq record is 4 lines
	print "\tTotal Reads in trimmed filtered FASTQ: $totalReads\n";

	### perfrom alignments ###
	# bwa aln [-n maxDiff] [-o maxGapO] [-e maxGapE] [-d nDelTail] [-i nIndelEnd] [-k maxSeedDiff] [-l seedLen] [-t nThrds] [-cRN] [-M misMsc] [-O gapOsc] [-E gapEsc] [-q trimQual] <in.db.fasta> <in.query.fq> > <out.sai>
	# if assay is deletion assay, then run different BWA parameters
	#'bwaMaxGaps:i', 'bwaMaxGapExts:i', 'bwaGapOpenPen:i', 'bwaGapExtPen:i', 'bwaMismatchPen:i'
	if ($assay =~ m/del/i) {
		$CMD = "$BWA aln -o $bwaMaxGaps -e $bwaMaxGapExts -O $bwaGapOpenPen -E $bwaGapExtPen -d 2 -i 2 -t $cpuCount $REF";
	}
	else {
		$CMD = "$BWA aln -M $bwaMismatchPen -t $cpuCount $REF";
	}
	$CMD = $CMD." $fastq > $outDir/".$name.".sai";
	print "\tRunning command: $CMD\n";
	print "\n";
	system($CMD);
	
	# bwa samse [-n maxOcc] <in.db.fasta> <in.sai> <in.fq> > <out.sam>
	$CMD = "$BWA samse $REF $outDir/$name".".sai $fastq > $outDir/".$name.".sam";
	print "\tRunning command: $CMD\n";
	print "\n";
	system($CMD);
	
	# extract unmapped reads
	$CMD = "$SAMTOOLS view -b -f 4 $outDir/".$name.".sam > $outDir/$name".".unmapped.bam";
	print "\tRunning command: $CMD\n";
	print "\n";
	system($CMD);
	#sleep(60);
	#$CMD = "$BEDTOOLS bamtofastq -i $outDir/$name".".unmapped.bam -fq $outDir/$name".".unmapped.fastq";
	$CMD = "$FASTQC -t $cpuCount $outDir/$name".".unmapped.bam";
	print "\tRunning command: $CMD\n";
	print "\n";
	system($CMD);
	#sleep(60);
	print "\n";
	
	# analyze unmapped reads to count primer-dimers
	$CMD = "$SAMTOOLS view $outDir/$name".".unmapped.bam";
	my @unmappedReadsSam = qx($CMD);
	chomp @unmappedReadsSam;
	my $primerDimerCount = countPrimerDimers(\@unmappedReadsSam,$primerFile,$assay);
	print "\tTotal primer-dimers found in unmapped reads: $primerDimerCount\n";
	
	# filter alignments and keep only primary alignments
	# samtools view -bSu out.sam  | samtools sort -  out.sorted
	$CMD = "$SAMTOOLS view -F 256 -q $MAPQmin -bSu $outDir/$name".".sam | $SAMTOOLS sort --threads $cpuCount -o $outDir/$name".".sorted.bam -";
	print "\tRunning command: $CMD\n";
	print "\n";
	system($CMD);
	
	#samtools index test_sorted.bam test_sorted.bai
	$CMD = "$SAMTOOLS index $outDir/$name".".sorted.bam $outDir/$name".".sorted.bai";
	print "\tRunning command: $CMD\n";
	print "\n";
	system($CMD);
	
	# mapped reads
	$CMD = "$SAMTOOLS view -F 4 $outDir/$name".".sorted.bam | wc -l";
	my $mappedReads = qx($CMD);
	#@s = split(/\s+/, $mappedReads);
	$mappedReads = int($mappedReads);
	print "\tMapped Reads: $mappedReads\n";
	
	# PhiX reads
	$CMD = "$SAMTOOLS view -F 4 $outDir/$name".".sorted.bam PhiX | wc -l";
	my $phixReads = qx($CMD);
	$phixReads = int($phixReads);
	print "\tPhiX Reads: $phixReads\n";
	
	# target reads (gene_seq)
	$CMD = "$SAMTOOLS view -F 4 $outDir/$name".".sorted.bam $target";
	#print "Running command: $CMD\n"; sleep (120);
	my @targetReadsSam = qx($CMD);
	chomp @targetReadsSam;
	#print join("\n",@targetReadsSam); sleep(120);
	my $targetReads = scalar(@targetReadsSam);
	print "\tTarget Reads: $targetReads\n";

	##### COUNT WT AND MT READS #####
	
	### count target region wild type reads if target region is more than just a SNP
	my $wtReads = 0;
	if ($targetStart != $targetEnd) {
		foreach my $samLine (@targetReadsSam) {
			chomp($samLine);
			#M03685:22:000000000-AEN0T:1:2113:5528:8081      0       chr7    55242447        37      57M     *       0       0       AATTCCCGTCGCTATCAAGGAATTAAGAGAAGCAACATCTCCGAAAGCCAACAAGGA       CCCCCGGGGGGGGGGGGGGG?FGEFFCCFGGGGGGGCEFGFGGGEGCFEGFFCDC<<        XT:A:U  NM:i:0  X0:i:1  X1:i:0  XM:i:0  XO:i:0  XG:i:0  MD:Z:57
			my @s = split("\t",$samLine);
			chomp($s[9]);
			if (index($s[9],$wtSeq,0) != -1) {
				$wtReads++;
			}
		}
		print "\tWildType Target Region Reads: $wtReads\n";
	}
	
	
	### only if deletion assay, count WT reads as those reads that do not have any deletions
	
	#if ($assay =~ m/del/i) {
		#foreach my $samLine (@targetReadsSam) {
			##M03685:22:000000000-AEN0T:1:2113:5528:8081      0       chr7    55242447        37      57M     *       0       0       AATTCCCGTCGCTATCAAGGAATTAAGAGAAGCAACATCTCCGAAAGCCAACAAGGA       CCCCCGGGGGGGGGGGGGGG?FGEFFCCFGGGGGGGCEFGFGGGEGCFEGFFCDC<<        XT:A:U  NM:i:0  X0:i:1  X1:i:0  XM:i:0  XO:i:0  XG:i:0  MD:Z:57
			#my @s = split("\t",$samLine);
			#my @t = split(":",$s[12]);
			#if ($s[5] !~ m/S|D/) {
				#$wtReads++;
			#}
		#}
		#print "\tWildType Reads: $wtReads\n";
	#}
	
	print "\n\t### FINDING MUTANT COUNTS BASED ON ALIGNMENT PILEUP ###\n\n";
	# get pileup info based on assay targets
	$CMD = "$BAMREADCOUNT -f $REF -l $mtCoordsFile $outDir/$name".".sorted.bam";
	print "\tRunning command: $CMD\n";
	my @readCounts = qx($CMD);
	chomp(@readCounts);
		
	# process read count pileup based on each mutant entry
	foreach my $mutantCoordsLine (@mutantsCoordsArr) {
		#chr	start	end	name	mutant
		my @mut = split("\t",$mutantCoordsLine);
		chomp($mut[4]);
		$mutCoordsCount{$mut[3]} = 0;
		foreach my $readCount (@readCounts) {
			chomp($readCount);
			#chr12	25398281	C	1795744	=...	A:154:25.86:30.36:25.86:0:154:0.87:0.06:63.95:0:0.00:31.00:0.42	C:1623256:36.77:37.61:36.77:0:1623256:0.87:0.03:37.64:0:0.00:31.00:0.42	G:47:31.64:30.87:31.64:0:47:0.87:0.05:46.68:0:0.00:31.00:0.42	T:172236:36.67:36.80:36.67:0:172236:0.87:0.03:37.64:0:0.00:31.00:0.42	N:0:0.00:0.00:0.00:0:0:0.00:0.00:0.00:0:0.00:0.00:0.00	-C:50:37.00:0.00:37.00:0:50:0.80:0.07:37.84:0:0.00:30.00:0.40	-CC:1:37.00:0.00:37.00:0:1:0.86:0.07:0.00:0:0.00:29.00:0.41
			my @read = split("\t",$readCount);
			if ($mut[0] =~ m/$read[0]/ && $mut[1] =~ m/$read[1]/) {
				my %countsHash;
				$countsHash{$mut[4]} = 0;
				for (my $i = 5; $i<(scalar(@read)); $i++) {
					my $line = $read[$i];
					chomp($line);
					my @s = split(':',$line);
					$countsHash{$s[0]} = $s[1];
				}
				
				my $countsHashTotal = 0;
				foreach my $key (keys %countsHash) {
					#if ($key =~ m/(^A|^C|^T|^G|^N)/i) {
						$countsHashTotal = $countsHashTotal + $countsHash{$key};
					#}
				}
				$countsHash{'N'} = $countsHashTotal;
				# IF deletion assay AND looking for deletions, then WT count = count from above
				if ($assay =~ m/del/i && $mut[1] != $mut[2] && $mut[3] =~ m/wt/i) {
					$countsHash{'N'} = int($wtReads);
				}
				# IF single SNP assay, then target WT equals SNP WT
				if ($targetStart == $targetEnd && $mut[3] =~ m/wt/i) {
					$wtReads = int($countsHash{$mut[4]});
				}
				
				$mutCoordsCount{$mut[3]} = int($countsHash{$mut[4]});
				last;
			}
		}
	}
	
	# other and ota counts
	my $otherCount = 0;
	$otherCount = int($totalReads) - int($mappedReads) - int($primerDimerCount);
	if ($otherCount<0) { $otherCount = 0; }
	print "\tOther Reads: $otherCount\n";
	my $otaCount = int($mappedReads) - int($targetReads) - int($phixReads);
	print "\tOTA_SUM: $otaCount\n";

	# summary stuff
	
	# print out sample specific file
	my $mutLine="";
	my $mutHeader="";
	foreach my $mutName (sort keys %mutCoordsCount) {
		$mutHeader = $mutHeader.",".$mutName;
		$mutLine = $mutLine.",".$mutCoordsCount{$mutName};
	}
	my $outFile = "$outDir/$name"."_rawCounts.csv";
	open(OUT, '>', $outFile) or die "ERROR: Could not open file $outFile $!\n";
	print OUT "Name,Assay,Input,ExpectedCN,RawReads,TrimmedReads,TrimmedFilteredReads,TotalMappedReads,TotalTargetReads,PhiXReads"."$mutHeader,PrimerDimer,OTHER,OTA_SUM,TargetWTReads\n";
	print OUT "$name,$assay,$input,$expectedCN,$rawReads,$trimmedReads,$totalReads,$mappedReads,$targetReads,$phixReads"."$mutLine,$primerDimerCount,$otherCount,$otaCount,$wtReads\n";
	close OUT;

	# build and push hash to summary cache array
	my %store = %mutCoordsCount;
	my %hash = (
		"lineNum" => int($sampleNumber),
		"runId" => $runId,
		"sampleNumber" => int($sampleNumber),
		"sampleName" => $sampleName,
		"totalReads" => int($totalReads), # total reads after adapter trimming and Qscore filtering
		"trimmedReads" => int($trimmedReads), # total reads after adapter trimming
		"totalSeq" => int($mappedReads), # total mapped reads
		"targetReads" => int($targetReads), # total mapped reads in target region
		"mutLine" => $mutLine,
		"mutCoordsCountRef" => \%store,
		"other" => int($otherCount),
		"OTA_SUM" => int($otaCount),
		"rawReads" => int($rawReads),
		"phixReads" => int($phixReads),
		"primerDimerReads" => int($primerDimerCount),
		"targetWtReads" => int($wtReads),
		"expectedCN" => $expectedCN,
		"input" => $input,
		"assay" => $assay,
		);
	
	## push to cacheOutHash
	#my $cacheOutRef = $cacheOutHash{$assay};
	#my @cacheOut = @$cacheOutRef;
	#push @cacheOut, \%hash;
	$cacheOutHash{$sampleNumber} = \%hash;
	
	if (!exists $mutHeaderHash{$assay}) { 
		$mutHeaderHash{$assay} = $mutHeader;
	}

	# clean up fastq_trimmed and SAM files
	$CMD = "rm $outDir/$name".".unmapped_fastqc.zip $outDir/".$name.".sai $outDir/".$name.".sam ".glob("$inDir/".$name."*_trimmed*")." ".glob("$inDir/".$name."*_trimmed_*");
	print "\tRunning command: $CMD\n";
	print "\n";
	system($CMD);
	
	print "===PROCESSING $name COMPLETE===\n";
	print "\n";
});
sleep(3);
### print out summary cache file
#foreach my $assay (sort keys %cacheOutHash) {
foreach my $assay (@assays) {
	my $mutHeader = $mutHeaderHash{$assay};
	my $outFile = "$outDir/$runId"."_$assay"."_RAWCOUNTS.csv";
	open(OUT, '>', $outFile) or die "ERROR: Could not open file $outFile $!\n";
	# print header
	print OUT "RunId,SampleNumber,Name,Assay,Input,ExpectedCN,RawReads,TrimmedReads,TrimmedFilteredReads,TotalMappedReads,TotalTargetReads,PhiXReads,PrimerDimer,OTHER,OTA_SUM,TargetWTReads"."$mutHeader\n";
	
	# loop thru cacheOutHash to print out samples with matching assays 
	foreach my $key (sort{$a<=>$b} keys %cacheOutHash) {
		my $hashRef = $cacheOutHash{$key};
		my %hash = %$hashRef;
		if ($hash{'assay'} eq $assay) {		
			print OUT "$hash{'runId'},$hash{'sampleNumber'},$hash{'sampleName'},$hash{'assay'},$hash{'input'},$hash{'expectedCN'},$hash{'rawReads'},$hash{'trimmedReads'},$hash{'totalReads'},$hash{'totalSeq'},$hash{'targetReads'},$hash{'phixReads'},$hash{'primerDimerReads'},$hash{'other'},$hash{'OTA_SUM'},$hash{'targetWtReads'}"."$hash{'mutLine'}\n";
		}
	}
	close OUT;

	print "\n";
	print "Summary out cache file: $outFile\n\n--- PROCESSING COMPLETE FOR ASSAY: $assay runId: $runId ---\n";
	print "\n";
	
	## PRINT PLOTS
	#IF L858R
	if ($assay =~ m/L858R/i) {
		$inputs{mtName} = "c2573_G";
		$inputs{wtName} = "c2573_T_WT";
		$inputs{allName} = "c2573_ALL";
	}
	elsif ($assay =~ m/Ex19Del/i) {
		$inputs{mtName} = "c.2235_2249del15";
		$inputs{wtName} = "c.2233_2256_WT";
		$inputs{allName} = "TotalTargetReads";
		
	}
	
	
	# DO PLOTTING
	if (defined $inputs{mtName} && exists $inputs{mtName} && defined $inputs{wtName} && exists $inputs{wtName} && defined $inputs{allName} && exists $inputs{allName}) {
		my $CMD = "$PLOT $outFile $inputs{mtName} $inputs{wtName} $inputs{allName}";
		print "\n";
		print "Generating plots...\n\tmtName: $inputs{mtName}\n\twtName: $inputs{wtName}\n\tallName: $inputs{allName}\n\n";
		print "\tRunning command: $CMD\n";
		system($CMD);
	}
	
	print "\n";
}
print "\n";
print "Start time: "; print join ' ', $dtStart->ymd, $dtStart->hms; print "\n";
my $dtEnd = DateTime->now;
print "End time: "; print join ' ', $dtEnd->ymd, $dtEnd->hms; print "\n";
my $span = DateTime::Format::Human::Duration->new();
print 'Total elapsed time: ', $span->format_duration_between($dtEnd, $dtStart); print "\n\n";










sub getFastqInfo {
	my $fastqName = shift;
	my @s = split("_",$fastqName);
	#MDJ88-U-R1-C1_S35_L001_R1_001_trimmed.fastq.gz
	my $sampleName = $s[0];
	my $sampleNumber = $s[1];
	$sampleNumber =~ s/S//i;
	
	return ($sampleName, $sampleNumber);
}

sub getAssayFromSampleSheetFile {
	my $sampleSheetFile = shift;
	my @sampleSheet;
	#my $assay = "NA";
	my %sampleAssayHash;
	open SS, '<', $sampleSheetFile;
	while (<SS>) {
		#my $doOut = 0;
		my $line = $_;
		chomp($line);
		next if ($line =~ m/^#/i);
		next if ($line eq "\r");
		#if ($doOut == 1) {
			push @sampleSheet, $line;
			#print "$line\n";
		#}
		#elsif ($line =~ /^Sample_ID/i) {
		#	$doOut = 1;
		#}
	}
	close SS;
	#Sample_ID,Sample_Name,Sample_Plate,Sample_Well,I7_Index_ID,index,GenomeFolder,Sample_Project,Description,Input,Source
	#Sample_ID,Sample_Name,Sample_Plate,Sample_Well,I7_Index_ID,index,Sample_Project,Description,Input,Source,Batch
	
	# figure out what line the header is on
	my $headerIdx = 0;
	for (my $i=0; $i<scalar(@sampleSheet); $i++) {
		my $line = $sampleSheet[$i];
		chomp($line);
		if ($line =~ m/^Sample_ID/) {
			$headerIdx = $i;
		}
	}
	#$headerIdx = $headerIdx + 1;
	#print "HEADER LINE IDX: $headerIdx\n"; sleep(120);
	
	# figure out what index the Sample_project field is
	my $idx = 0;
	my $inputIdx = 0;
	my @s = split(",",$sampleSheet[$headerIdx]);
	if ($sampleSheet[$headerIdx] !~ m/^sample_id/i) {
		$headerIdx++;
		@s = split(",",$sampleSheet[$headerIdx]);
	}
	for (my $i=0; $i<scalar(@s); $i++) {
		if ($s[$i] =~ m/sample_project/i) {
			# figure out what index the Sample_project field is
			$idx = $i;
			#print "\n\tSample_Project found at $idx\n\n";
			#last;
		}
		
		elsif ($s[$i] =~ m/input/i) {
			$inputIdx = $i;
		}
	}
	
	for (my $i = ($headerIdx+1); $i<scalar(@sampleSheet); $i++) {
		next if ($sampleSheet[$i] =~ m/^#/i);
		my @s = split(",",$sampleSheet[$i]);
		#Sample_ID,Sample_Name,Sample_Plate,Sample_Well,I7_Index_ID,index,GenomeFolder,Sample_Project,Description,Input,Source
		#Sample_ID,Sample_Name,Sample_Plate,Sample_Well,I7_Index_ID,index,Sample_Project,Description,Input,Source,Batch
		my $assay = "NA";
		if ($s[$idx] =~ /(kras|G12)/i) {
			$assay = "KRAS_G12X"; }
		elsif ($s[$idx] =~ /ex19|exon19|del/i) {
			$assay = "EGFR_EX19DEL"; }
		elsif ($s[$idx] =~ /l858/i) {
			$assay = "EGFR_L858R"; }
		elsif ($s[$idx] =~ /t790/i) {
			$assay = "EGFR_T790M"; }
		elsif ($s[$idx] =~ /braf/i) {
			$assay = "BRAF_V600X"; }
		else { warn "WARNING: Could not find assay for sample line $i in $sampleSheetFile\n"; next; }
		$sampleAssayHash{$s[0]}{'assay'} = $assay;
		
		if ($inputIdx != 0) {
			$sampleAssayHash{$s[0]}{'input'} = int($s[$inputIdx]);
		}
		else {
			$sampleAssayHash{$s[0]}{'input'} = " ";
		}
	}
	
	return \%sampleAssayHash;
	
	#print "FOUND ASSAY: $assay\n"; sleep(120);
}

sub getTargetFromAssay {
	my $assay = shift;
	my $target = "NA";
	my $wtSeq = "NA";
	foreach my $line (@targets) {
		next if ($line =~ /^#/);
		#chr	start	stop	name	wtSeq
		my @s = split("\t",$line);
		if ($s[3] =~ m/$assay/i) {
			$target = "$s[0]:$s[1]-$s[2]";
			chomp($s[4]);
			$wtSeq = $s[4];
			last;
		}
	}
	return ($target, $wtSeq);
}


sub getMutantsCoords {
	my $assay = shift;
	my $file = "NA";
	my @mutantsCoords;
	if ($assay =~ /kras_g/i) {
		$file = "/home/palak/isilon/KRAS_G12X_mutants_coords.bed"; }
	elsif ($assay =~ /ex19|exon19|del/i) {
		$file = "/home/palak/isilon/EGFR_EX19DEL_mutants_coords.bed"; }
	elsif ($assay =~ /l858/i) {
		$file = "/home/palak/isilon/EGFR_L858R_mutants_coords.bed"; }
	elsif ($assay =~ /t790/i) {
		$file = "/home/palak/isilon/EGFR_T790M_mutants_coords.bed"; }
	elsif ($assay =~ /braf/i) {
		#$file = "/home/palak/isilon/BRAF_V600X_mutants_coords.bed"; 
		}
	
	if ($file ne "NA") {
		$file = abs_path(glob($file));
		open MT, '<', $file;
		while (<MT>) {
			next if ($_ =~ m/^#/);
			push @mutantsCoords, $_;
		}
		close MT;
	}
	return (\@mutantsCoords, $file);
}


sub countPrimerDimers {
	my ($samRef, $primerFile, $assay) = @_;
	my @sam = @$samRef;
	$primerFile = abs_path(glob($primerFile));
	my $forwardPrimer = "";
	my $reversePrimer = "";
	
	my $primerDimerCount = 0;
	
	# open primer file and find forward/reverse primers based on assay
	open PRIMER, '<', $primerFile;
	chomp(my @primerLines = <PRIMER>);
	close PRIMER;
	
	for (my $i=0; $i<scalar(@primerLines); $i+=2) {
		my $header = $primerLines[$i];
		if ($header =~ m/$assay/i && $header =~ m/forward/i) {
			$forwardPrimer = $primerLines[$i+1];
		}
		elsif ($header =~ m/$assay/i && $header =~ m/reverse/i) {
			$reversePrimer = $primerLines[$i+1];
		}
	}
	
	# loop through unmapped reads SAM file to find primer-dimers
	foreach my $samLine (@sam) {
		#M03685:22:000000000-AEN0T:1:2113:5528:8081      0       chr7    55242447        37      57M     *       0       0       AATTCCCGTCGCTATCAAGGAATTAAGAGAAGCAACATCTCCGAAAGCCAACAAGGA       CCCCCGGGGGGGGGGGGGGG?FGEFFCCFGGGGGGGCEFGFGGGEGCFEGFFCDC<<        XT:A:U  NM:i:0  X0:i:1  X1:i:0  XM:i:0  XO:i:0  XG:i:0  MD:Z:57
		my @s = split("\t",$samLine);
		my $read = $s[9];
				
		# get index of where the forward primer matches
		my $forwardIndex = aindex("$forwardPrimer", ["i 20%"], $read);
		# get index of where the reverse primer matches
		my $reverseIndex = aindex("$reversePrimer", ["i 20%"], $read);
		
		# if both primers are found....
		#if ($forwardIndex != -1 && $reverseIndex != -1) {
		#	my $forwardEnd = $forwardIndex + length($forwardPrimer);
		#	if (abs($reverseIndex - $forwardEnd) <= 3) {
		#		$primerDimerCount++;
		#	}
		#}
		# if any one of the primers are found...
		if ($forwardIndex != -1 || $reverseIndex != -1) {
			#my $forwardEnd = $forwardIndex + length($forwardPrimer);
			#if (abs($reverseIndex - $forwardEnd) <= 3) {
				$primerDimerCount++;
			#}
		}
	}
	
	return $primerDimerCount;
}



sub Usage {
	print "Usage: perl generate_RAWCOUNTS.pl --indir /path/to/run [--outdir /path/to/outfolder] [--cpus N cores per job] [--QscoreMin N] [--MAPQmin N] [--mismatch 0.N] [--percentBases N] [--bwaMaxGaps N] [--bwaMaxGapExts N] [--bwaGapOpenPen N] [--bwaGapExtPen N] [--bwaMismatchPen N]\n";
}
